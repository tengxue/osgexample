﻿// ***********************************************************************
// Assembly         : TxOsgViewer
// Author           : tian_
// Created          : 02-08-2022
//
// Last Modified By : tian_
// Last Modified On : 02-08-2022
// ***********************************************************************
// <copyright file="MatrixTransformTestCmd.cs" company="北京腾雪科技有限责任公司">
//     Copyright © 北京腾雪科技有限责任公司 2022
// </copyright>
// <summary></summary>
// ***********************************************************************
using Osg;
using Tx;
using Tx.OSG.UI;
using Tx.OSG.UI.Commands;

namespace OsgCommon.Cmds
{
    /// <summary>
    /// Class MatrixTransformTestCmd.
    /// Implements the <see cref="Tx.IExtendCommand" />
    /// </summary>
    /// <seealso cref="Tx.IExtendCommand" />
    [OsgViewCmdEnable]
    public class MatrixTransformTestCmd : IExtendCommand
    {
        /// <summary>
        /// Executes the specified command data.
        /// </summary>
        /// <param name="cmdData">The command data.</param>
        /// <param name="message">The message.</param>
        /// <returns>CmdResult.</returns>
        public CmdResult Execute(ExtendCmdData cmdData, ref string message)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgObj = (viewWindow.View as IOsgView).OsgObj;

            var vertexArray = new Vec3Array();
            vertexArray.push_back(new Vec3f(-1, -1, 0));
            vertexArray.push_back(new Vec3f(1, -1, 0));
            vertexArray.push_back(new Vec3f(1, 1, 0));
            vertexArray.push_back(new Vec3f(-1, 1, 0));

            var normalArray = new Vec3Array();
            normalArray.push_back(new Vec3f(0, 0, 1));

            var colorArray = new Vec4Array();
            colorArray.push_back(new Vec4f(1, 1, 1, 1));

            var geometry = new Geometry();
            geometry.VertexArray = vertexArray;
            geometry.setNormalArray(normalArray, Osg.Array.Binding.BIND_PER_PRIMITIVE_SET);
            geometry.setColorArray(colorArray, Osg.Array.Binding.BIND_PER_PRIMITIVE_SET);
            geometry.addPrimitiveSet(new DrawArrays((uint)PrimitiveSet.Mode.QUADS, 0, 4));


            var geode = new Geode();
            geode.addDrawable(geometry);

            // 上面部分变, 下面部分只变换位置
            var mat1 = Matrixf.rotate(new Vec3f(0, 0, 1), new Vec3f(0, 1, 0)) * Matrixf.translate(Vec3d.X_AXIS);
            var matrixTransform1 = new MatrixTransform(mat1);
            matrixTransform1.addChild(geode);

            var mat2 = Matrixf.rotate(new Vec3f(0, 0, 1), new Vec3f(0, 1, 0)) * Matrixf.translate(new Vec3d(2, 0, 0));
            var matrixTransform2 = new MatrixTransform(mat2);
            matrixTransform2.addChild(geode);

            var group = new Group(){Name = "1"};
            @group.addChild(matrixTransform1);
            @group.addChild(matrixTransform2);

            osgObj.UpdateModel("Models", group);
            osgObj.SetView(ViewMode.ShowAll);

            return CmdResult.Succeed;
        }
    }
}
