﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Osg;
using Tx.OSG.UI;
using Tx.OSG;
using Tx;

namespace OsgCommon.Cmds
{
    public static class GeometryCmds
    {
        /// <summary>
        /// 新建一个立方体
        /// </summary>
        /// <param name="cmdData"></param>
        /// <returns></returns>
        public static CmdResult NewBox(ExtendCmdData cmdData)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgObj = (viewWindow.View as IOsgView).OsgObj;

            var geometry = OsgGeometry.NewBox(-100, 100, 100, 0, 0, 0, BoxFace.All);
            geometry.SetColor(new Vec4f(1, 1, 1, 1));
            var geode = geometry.NewGeode("3");
            osgObj.UpdateModel("Box", geode);
            osgObj.SetView(ViewMode.ShowAll);

            return CmdResult.Succeed;
        }
        /// <summary>
        /// 新建一个面
        /// </summary>
        /// <param name="cmdData"></param>
        /// <returns></returns>
        public static CmdResult NewPolygon(ExtendCmdData cmdData)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgObj = (viewWindow.View as IOsgView).OsgObj;

            var vec3array = new Vec3Array();
            vec3array.push_back(new Vec3f(0, 0, 0));
            vec3array.push_back(new Vec3f(1, 0, 0));
            vec3array.push_back(new Vec3f(1, 1, 0));
            vec3array.push_back(new Vec3f(0, 1, 0));
            var geometry = OsgGeometry.NewPolygon(vec3array, Vec3f.Z_AXIS, OsgColor.Red);

            var vec3array2 = new Vec3Array();
            vec3array2.push_back(new Vec3f(1, 0, 0));
            vec3array2.push_back(new Vec3f(1, 1, 0));
            vec3array2.push_back(new Vec3f(1, 1, 1));
            vec3array2.push_back(new Vec3f(1, 0, 1));
            var geometry2 = OsgGeometry.NewPolygon(vec3array2, Vec3f.X_AXIS, OsgColor.Green);

            var geode = new[] {geometry, geometry2}.NewGeode("Polygon");
            geode.getOrCreateStateSet().SetTwoSided(true);

            //OsgDB.Static.writeNodeFile(geode, "D:/test.osgb");

            osgObj.UpdateModel("Models", geode);
            osgObj.SetView(ViewMode.ShowAll);

            return CmdResult.Succeed;
        }
        /// <summary>
        /// 新建一个矩形圆环
        /// </summary>
        /// <param name="cmdData"></param>
        /// <returns></returns>
        public static CmdResult NewRectAnnulus(ExtendCmdData cmdData)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgObj = (viewWindow.View as IOsgView).OsgObj;

            // 创建一个举行圆环
            var geometry = OsgGeometry.NewRectAnnulus(new Vec2f(), 100, 150, 50,
                0, (float)Math.PI * 0.5f, (float)Math.PI / 48, BoxFace.All, Color.White.ToVec4f());
            var geode = geometry.NewGeode("RectAnnulus");
            osgObj.UpdateModel("Models", geode);
            osgObj.SetView(ViewMode.ShowAll);

            return CmdResult.Succeed;
        }
    }
}
