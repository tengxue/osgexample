﻿// ***********************************************************************
// Assembly         : OsgCommon.Cmds
// Author           : tian_
// Created          : 2022/5/16 0:02:16
//
// Last Modified By : tian_
// Last Modified On : 2022/5/16 0:02:16
// ***********************************************************************
// <copyright file="AddStatsHandlerCmd.cs" company="北京腾雪科技有限责任公司">
//     Copyright © 北京腾雪科技有限责任公司 2022
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tx;
using Tx.IO;
using Tx.OSG;
using Tx.OSG.UI;
using Tx.UI;

namespace OsgCommon.Cmds
{
    /// <summary>
    /// Class AddStatsHandlerCmd.
    /// </summary>
    /// <seealso cref="Tx.IExtendCommand" />
	public class LoadOsgFileCmd : IExtendCommand
    {
        /// <summary>
        /// Executes the specified command data.
        /// </summary>
        /// <param name="cmdData">The command data.</param>
        /// <param name="message">The message.</param>
        /// <returns>CmdResult.</returns>
        public CmdResult Execute(ExtendCmdData cmdData, ref string message)
        {
            var mainForm = cmdData.MainWindow as IMainWindow;
            var viewForm = cmdData.ViewWindow as IViewWindow;
            var osgView = viewForm.View as IOsgView;
            var osgObj = osgView.OsgObj;

            if (!FileFilter.OsgAll.Open(out string[] osgFileNames))
            {
                return CmdResult.Cancel;
            }

            foreach (var osgFileName in osgFileNames)
            {
                var node = OsgDBEx.ReadNodeFile(osgFileName);
                if (node.IsValid())
                {
                    osgObj.UpdateModel("Models", node, Path.GetFileNameWithoutExtension(osgFileName));

                }
                else
                {
                    cmdData.App.Singleton<MessageProxy>().ShowError(string.Format("打开文件\"{0}\"失败！", osgFileName));
                }
            }

            return CmdResult.Succeed;
        }
    }
}
