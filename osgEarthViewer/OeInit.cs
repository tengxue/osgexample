﻿using OsgEarth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace osgEarthViewer
{
    internal class OeInit
    {
        public static void Run(string[] args)
        {
            var usage = delegate (string name) { Console.WriteLine($"\nUsage:{name} file.earth"); };

            OsgEarth._.initialize();

            var arguments = new Osg.ArgumentParser(args);

            // help?
            if (arguments.read("--help"))
            {
                usage(args[0]);
                return;
            }

            // create a viewer:
            OsgViewer.Viewer viewer = new OsgViewer.Viewer(arguments);

            // Tell the database pager to not modify the unref settings
            viewer.DatabasePager.setUnrefImageDataAfterApplyPolicy(true, false);

            // thread-safe initialization of the OSG wrapper manager. Calling this here
            // prevents the "unsupported wrapper" messages from OSG
            //OsgDB.Registry.instance().getObjectWrapperManager().findWrapper("osg::Image");

            // install our default manipulator (do this before calling load)
            viewer.CameraManipulator = new EarthManipulator(arguments);

            // disable the small-feature culling
            viewer.Camera.SmallFeatureCullingPixelSize = -1.0f;

            // load an earth file, and support all or our example command-line options
            // and earth file <external> tags
            var node = new MapNodeHelper().load(arguments, viewer);
            if (node != null)
            {
                viewer.SceneData = node;
                OsgEarth.Util.Metrics.run(viewer);
            }
            else
            {
                usage(args[0]);
            }
        }
    }
}
