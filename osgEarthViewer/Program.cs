﻿
namespace osgEarthViewer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            OsgConfiguration.Configure();
            // 在Main函数中使用了osg相关的类(或者using)的话，就会执行加载osg.NET.dll，
            // 但是还没有执行 OsgConfiguration.Configure函数，导致找不到依赖的dll，
            // 所以需要移动实现到其他类里
            OeInit.Run(args);
        }
    }
}
