﻿
using System.Windows;

namespace OsgEarthExample
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        static App()
        {
            OsgConfiguration1.Configure();
        }
    }
}
