using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OsgEarthExample
{
    public static class OsgConfiguration1
    {
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool SetDefaultDllDirectories(uint directoryFlags);
        //               LOAD_LIBRARY_SEARCH_DEFAULT_DIRS
        private const uint DllSearchFlags = 0x00001000;

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AddDllDirectory(string lpPathName);

        /// <summary>
        /// Construction of Gdal/Ogr
        /// </summary>
        static OsgConfiguration1()
        {
            string executingDirectory = string.Empty, osgPath = string.Empty, nativePath = string.Empty;
            try
            {
                string executingAssemblyFile = new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath;
                executingDirectory = Path.GetDirectoryName(executingAssemblyFile);

                if (string.IsNullOrEmpty(executingDirectory))
                    throw new InvalidOperationException("cannot get executing directory");


                // modify search place and order
                SetDefaultDllDirectories(DllSearchFlags);

                osgPath = Path.Combine(executingDirectory, "osg");
                nativePath = Path.Combine(osgPath, GetPlatform());
                if (!Directory.Exists(nativePath))
                    throw new DirectoryNotFoundException($"OSG native directory not found at '{nativePath}'");
                if (!File.Exists(Path.Combine(nativePath, "osg161-osg.dll")))
                    throw new FileNotFoundException(
                        $"OSG native wrapper file not found at '{Path.Combine(nativePath, "osg161-osg.dll")}'");

                // Add directories
                AddDllDirectory(nativePath);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e, "error");
                Trace.WriteLine($"Executing directory: {executingDirectory}", "error");
                Trace.WriteLine($"osg directory: {osgPath}", "error");
                Trace.WriteLine($"native directory: {nativePath}", "error");

                //throw;
            }
        }
        

        /// <summary>
        /// 
        /// </summary>
        public static void Configure()
        {
        }
        
        /// <summary>
        /// Function to determine which platform we're on
        /// </summary>
        private static string GetPlatform()
        {
            return Environment.Is64BitProcess ? "x64" : "x86";
        }
    }
}