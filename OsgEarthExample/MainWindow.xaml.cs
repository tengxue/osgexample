﻿using Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Tx;
using System.Windows.Interop;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Tx.FluentEx;
using Tx.osgEarth.UI.Commands.ViewCmds;
using OsgEarthCommon;
using Tx.OSG.UI.Commands;
using Tx.osgEarth.UI;
using Tx.UI;
using Tx.Win;
using Tx.WPF;

namespace OsgEarthExample
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : RibbonWindow, IMainWindow, IViewWindow
    {
        public static MainWindow Instance { get; private set; }

        public AppService App { get; private set; }

        public Ribbon Ribbon => ribbon;

        public OsgEarthCtrl osgEarthView { get; private set; }
        public MainWindow()
        {

            Instance = this;
            InitAppService();

            InitializeComponent();

            osgEarthView = new OsgEarthCtrl(Path.Combine(Assembly.GetCallingAssembly().FolderPath(), "base.earth"));
            osgEarthView.Dock = DockStyle.Fill;
            osgEarthViewPanel.Controls.Add(osgEarthView);

            InitRibbon();
            
            ViewWindowActive?.Invoke(this);
        }

        private void InitRibbon()
        {
            var ribbonCreater = new RibbonBuilder(ribbon, App);
            ribbonCreater.Tab("地球").Group("视图")
                .AddBotton<FlyToCmd>("飞往", "/Resources/paper-plane-icon.png");
            ribbonCreater.Tab("地球").Group("图层")
                //.AddBotton<HideOrShowTiandituCiaCmd>("天地图矢量", "/Resources/maps.png")
                .AddBotton(Cmds.AddImageLayer, "添加影像", "/Resources/maps.png")
                .AddBotton(Cmds.AddElevationLayer, "添加高程", "/Resources/maps.png");
            ribbonCreater.Tab("测试").Group("渲染")
                .AddBotton<AddStatsHandlerCmd>("渲染信息", "/Resources/0_48px.png")
                .AddBotton<AddStateSetManipulatorCmd>("渲染模式", "/Resources/1_48px.png");
        }

        private void InitAppService()
        {
            App = new AppService() { MainWindow = this };
            // 绑定预定义的消息
            App.SingletonSet(MessageBoxEx.MessageProxy);
            App.Singleton<ProgressProxy>().OnSetText += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    bool bRefresh = false;
                    if (args.Caption != null && tbInfo.Text != args.Caption)
                    {
                        tbInfo.Text = args.Caption;
                        bRefresh = true;
                    }

                    //                     if (bRefresh)
                    //                     {
                    //                         Application.DoEvents();
                    //                     }
                });
            };
            App.Singleton<ProgressProxy>().OnSetValue += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    //                     bool bRefresh = false;
                    //                     if (value > 0 && tsProgressBar.Value != value)
                    //                     {
                    TaskbarManager.Instance.SetProgressValue(args.Value, 100, this.Handle);
                    // 
                    //                         tsProgressBar.Value = value;
                    //                         bRefresh = true;
                    //                     }
                    // 
                    //                     if (bRefresh)
                    //                     {
                    //                         statusStrip.Refresh();
                    //                         Application.DoEvents();
                    //                     }
                });
            };
            App.Singleton<ProgressProxy>().OnClear += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    TaskbarManager.Instance.SetProgressValue(0, 100, this.Handle);

                    tbInfo.Text = "就绪";
                    //tsProgressBar.Value = 0;
                    //statusStrip.Refresh();
                    //Application.DoEvents();
                });
            };
        }

        #region IWindow

        /// <summary>Window句柄.</summary>
        /// <value>The handle.</value>
        public IntPtr Handle  => ((HwndSource)PresentationSource.FromDependencyObject(this)).Handle;

        /// <summary>图标.</summary>
        /// <value>The icon.</value>
        public object IconImage
        {
            get
            {
                return this.Icon;
            }
            set
            {
                this.Icon = value as ImageSource;
            }
        }

        #endregion

        #region IMainWindow


        /// <summary>Gets the active view window.</summary>
        /// <value>The active view window.</value>
        public object ActiveViewWindow => this;

        /// <summary>更新UI.</summary>
        public void RefreshUI()
        {
            this.InvalidateVisual();
        }

        /// <summary>Occurs when [view window active].</summary>
        public event Action<IViewWindow> ViewWindowActive;

        #endregion

        #region IViewWindow
        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <value>The parent.</value>
        public object ParentWindow => this.Owner;

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public object View => osgEarthView;

        #endregion
    }
}
