﻿using Osg;
using OsgAnimation;
using OsgUtil;
using OsgViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorphGeometryTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var channel = new FloatLinearChannel();
            channel.Name = "0";
            channel.TargetName = "MorphCallback";
            createMorphKeyframes(channel.getOrCreateSampler().getOrCreateKeyframeContainer());
            var anim = new Animation();
            anim.playMode = Animation.PlayMode.PPONG;
            anim.addChannel(channel);

            var mng = new BasicAnimationManager();
            mng.registerAnimation(anim);

            var morph = new MorphGeometry(createSourceGeometry());
            morph.addMorphTarget(createTargetGeometry());

            var geode = new Geode();
            geode.addDrawable(morph);
            var updateMorph = new UpdateMorph("MorphCallback");
            updateMorph.addTarget("MorphCallback");
            geode.UpdateCallback = updateMorph;

            var root = new Group();
            root.addChild(geode);
            root.UpdateCallback = mng;

            mng.playAnimation(anim);

            mng.stopAll();

            var viewer = new Viewer();
            viewer.SceneData = root;
            viewer.realize();
            viewer.run();
        }

        static Program()
        {
            OsgConfiguration.Configure();
        }
        static Geometry createSourceGeometry()
        {
            var vertices = new Vec3Array();
            vertices.push_back(new Vec3f(0.0f, 5.0f, -2.5f));
            vertices.push_back(new Vec3f(0.0f, 0.0f, -2.5f));
            vertices.push_back(new Vec3f(2.5f, 5.0f, 0.0f));
            vertices.push_back(new Vec3f(2.5f, 0.0f, 0.0f));
            vertices.push_back(new Vec3f(5.0f, 5.0f, -2.5f));
            vertices.push_back(new Vec3f(5.0f, 0.0f, -2.5f));

            var geom = new Geometry();
            geom.VertexArray = vertices;
            geom.addPrimitiveSet(new DrawArrays(PrimitiveSet.Mode.QUAD_STRIP, 0, 6));

            SmoothingVisitor.smooth(geom);
            return geom;
        }

        static Geometry createTargetGeometry()
        {
            var vertices = new Vec3Array();
            vertices.push_back(new Vec3f(0.0f, 5.0f, 1.0f));
            vertices.push_back(new Vec3f(0.0f, 0.0f, 1.0f));
            vertices.push_back(new Vec3f(2.5f, 5.0f, -1.0f));
            vertices.push_back(new Vec3f(2.5f, 0.0f, -1.0f));
            vertices.push_back(new Vec3f(5.0f, 5.0f, 1.0f));
            vertices.push_back(new Vec3f(5.0f, 0.0f, 1.0f));

            var geom = new Geometry();
            geom.VertexArray = vertices;
            geom.addPrimitiveSet(new DrawArrays(PrimitiveSet.Mode.QUAD_STRIP, 0, 6));

            SmoothingVisitor.smooth(geom);

            return geom;
        }

        static void createMorphKeyframes(FloatKeyframeContainer container)
        {
            container.push_back(new FloatKeyframe(0.0f, 0.0f));
            container.push_back(new FloatKeyframe(2.0f, 1.0f));
        }

    }
}
