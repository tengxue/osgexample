﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tx.OSG.UI.Commands;
using Tx.osgEarth.UI.Commands.ViewCmds;
using Tx.osgEarth.UI;
using Tx.UI;

namespace TxOsgEarthView
{
    internal class OsgEarthRun
    {
        public static void Run()
        {
            var earthViewSimpleForm = new SimpleOsgEarthViewForm(Path.Combine(Application.StartupPath, "base.earth"));
            var osgEarthObj = earthViewSimpleForm.ViewCtrl.OsgEarthObj;

            var map = osgEarthObj.MapNode.Map;
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new TianDiTu(TianDiTu.Layer.img)), Name = nameof(TianDiTu.Layer.img) });
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new TianDiTu(TianDiTu.Layer.cia)), Name = nameof(TianDiTu.Layer.cia) });
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new BingMap(BingMap.Layer.SatelliteMap)), Name = nameof(BingMap.Layer.SatelliteMap) });
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new AMap(AMap.Layer.SatelliteMap)), Name = nameof(AMap.Layer.SatelliteMap) });
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new AMap(AMap.Layer.MAP)), Name = nameof(AMap.Layer.MAP) });
            //map.addLayer(new TileSourceImageLayer() { TileSource = new WebTileSource(new AMap(AMap.Layer.Road)), Name = nameof(AMap.Layer.Road) });

            earthViewSimpleForm.menuStrip.Items.Item<ToolStripMenuItem>("视点").DropDownItems
                .AddItem<ToolStripMenuItem, FlyToCmd>("飞往", earthViewSimpleForm.App)
                .AddItem<ToolStripMenuItem, WeiZhiHuiZhengCmd>("回正", earthViewSimpleForm.App);

            earthViewSimpleForm.menuStrip.Items.Item<ToolStripMenuItem>("状态").DropDownItems
                .AddItem<ToolStripMenuItem, AddStatsHandlerCmd>("AddStatsHandler", earthViewSimpleForm.App)
                .AddItem<ToolStripMenuItem, AddStateSetManipulatorCmd>("AddStateSetManipulator", earthViewSimpleForm.App);

            Application.Run(earthViewSimpleForm);
        }

        //         public static ImageLayer CreateColorRampLayer()
        //         {
        //             var readFileCallback = new FunReadFileCallback();
        //             readFileCallback.OnReadObject += delegate (string s, Options options)
        //             {
        //                 if (s.Contains(".osgearth_GoogleEarthDem"))
        //                 {
        //                     return new ReaderWriter.ReadResult(new WebTileSource(MapTileType.TIANDITU_DEM));
        //                 }
        //                 return OsgDB.Registry.instance().readObjectImplementation(s, options);
        //             };
        //             OsgDB.Registry.instance().setReadFileCallback(readFileCallback);
        // 
        //             var colorRampOptions = new ColorRampOptions();
        //             colorRampOptions.elevationLayer = new ElevationLayerOptions("", new TileSourceOptions() { Driver = "GoogleEarthDem" });
        //             colorRampOptions.ramp = new URI(@"D:\SDK\OpenSceneGraph\osgearth-2.10.1\data\colorramps\elevation.clr");
        //             var tileSource = TileSourceFactory.create(colorRampOptions);
        //             tileSource.open();
        //             OsgDB.Registry.instance().setReadFileCallback(null);
        // 
        //             var imageLayer = new ImageLayer(new ImageLayerOptions(), tileSource);
        // 
        //             return imageLayer;
        //         }
    }
}
