﻿// ***********************************************************************
// Assembly         : TxOsgEarthView
// Author           : tian_
// Created          : 02-08-2022
//
// Last Modified By : tian_
// Last Modified On : 02-08-2022
// ***********************************************************************
// <copyright file="Program.cs" company="">
//     Copyright ©  2022
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Windows.Forms;

namespace TxOsgEarthView
{
    /// <summary>
    /// Class Program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            OsgConfiguration.Configure();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            OsgEarthRun.Run();
        }
    }
}
