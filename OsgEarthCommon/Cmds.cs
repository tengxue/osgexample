﻿using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tx;
using Tx.IO;
using Tx.osgEarth;
using Tx.osgEarth.UI;
using Tx.osgEarth.UI.Commands;
using Tx.UI;

namespace OsgEarthCommon
{
    public static class Cmds
    {
        [EarthViewCmdEnable]
        public static CmdResult AddElevationLayer(ExtendCmdData cmdData)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgEarthView = viewWindow.View as IOsgEarthView;

            if (FileFilter.Tif.Open(out string filePath))
            {
                var elevationLayer = OeLayer.NewGDALElevationLayer(filePath);
                osgEarthView.OsgEarthObj.AddLayer(elevationLayer);
            }
            return CmdResult.Succeed;
        }
        [EarthViewCmdEnable]
        public static CmdResult AddImageLayer(ExtendCmdData cmdData)
        {
            var viewWindow = cmdData.ViewWindow as IViewWindow;
            var osgEarthView = viewWindow.View as IOsgEarthView;

            if (FileFilter.Tif.Open(out string filePath))
            {
                var imageLayer = OeLayer.NewGDALImageLayer(filePath, Path.GetFileNameWithoutExtension(filePath));
                osgEarthView.OsgEarthObj.AddLayer(imageLayer);
            }
            return CmdResult.Succeed;
        }
    }
}
