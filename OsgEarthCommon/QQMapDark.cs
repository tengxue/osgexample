﻿// ***********************************************************************
// Assembly         : OsgEarthCommon
// Author           : tian_
// Created          : 03-24-2022
//
// Last Modified By : tian_
// Last Modified On : 03-29-2022
// ***********************************************************************
// <copyright file="QQMapDark.cs" company="北京腾雪科技有限责任公司">
//     Copyright © 北京腾雪科技有限责任公司 2022
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tx.osgEarth;
using OsgEarth;
using Tx;

namespace OsgEarthCommon
{
    /// <summary>
    /// Class QQMapDark.
    /// Implements the <see cref="Tx.osgEarth.TileProvider" />
    /// </summary>
    /// <seealso cref="Tx.osgEarth.TileProvider" />
//     public class QQMapDark : TileProvider
//     {
//         /// <summary>
//         /// The road
//         /// </summary>
//         private const string road = "https://rt{0}.map.gtimg.com/tile?z={1}&x={2}&y={3}&styleid=4";
// 
//         /// <summary>
//         /// Initializes a new instance of the <see cref="QQMapDark"/> class.
//         /// </summary>
//         public QQMapDark()
//         {
//             this.Name = nameof(QQMapDark) ;
//             this.Profile = Profile.create("spherical-mercator", "", 1, 1);
//         }
// 
//         /// <summary>
//         /// Gets the url.
//         /// </summary>
//         /// <param name="l">The l.</param>
//         /// <param name="x">The x.</param>
//         /// <param name="y">The y.</param>
//         /// <returns>A string.</returns>
//         public override string GetUrl(uint l, uint x, uint y)
//         {
//             y = (uint)Math.Pow(2, l) - y - 1;
//             return string.Format(road, RandomEx.Instance.Next(4), l, x, y);
//         }
//     }
}
