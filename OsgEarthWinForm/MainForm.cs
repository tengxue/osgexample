﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OsgEarth;
using OsgEarth.Contrib;
using OsgEarthCommon;
using Tx;
using Tx.OSG.UI;
using Tx.OSG.UI.Commands;
using Tx.osgEarth;
using Tx.osgEarth.UI;
using Tx.osgEarth.UI.Commands.ViewCmds;
using Tx.UI;
using Tx.UI.Forms;
using Tx.Win;

namespace OsgEarthWinForm
{
    public partial class MainForm : System.Windows.Forms.Form, IMainWindow, IViewWindow
    {
        public AppService App { get; private set; }
        private OsgEarthCtrl osgEarthView;
        public MainForm()
        {
            InitAppService();

            //var wktString = "EPSG:3857";			//web墨卡托投影
            //string wktString = "EPSG:4326";			//wgs84
            //var profileOpts = new ProfileOptions();
            //profileOpts.srsString = wktString;

            //var options = new Map.Options();
            //options.profile = profileOpts;


            osgEarthView = new OsgEarthCtrl(Path.Combine(Application.StartupPath, "base.earth"), OsgRenderMode.Thread);
            osgEarthView.Dock = DockStyle.Fill;
            this.Controls.Add(osgEarthView);

            InitializeComponent();

            InitMenu();

            ViewWindowActive?.Invoke(this);
        }

        private void InitMenu()
        {
            menuStrip.Items.Item<ToolStripMenuItem>("视点").DropDownItems
                .AddItem<ToolStripMenuItem, FlyToCmd>("飞往", App)
                .AddItem<ToolStripMenuItem, WeiZhiHuiZhengCmd>("回正", App);

            menuStrip.Items.Item<ToolStripMenuItem>("状态").DropDownItems
                .AddItem<ToolStripMenuItem, AddStatsHandlerCmd>("AddStatsHandler", App)
                .AddItem<ToolStripMenuItem, AddStateSetManipulatorCmd>("AddStateSetManipulator", App);
        }
        private void InitAppService()
        {
            App = new AppService() { MainWindow = this };
            // 绑定预定义的消息
            App.SingletonSet(MessageBoxEx.MessageProxy);
            App.Singleton<ProgressProxy>().OnSetText += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    bool bRefresh = false;
                    if (args.Caption != null && tbInfo.Text != args.Caption)
                    {
                        tbInfo.Text = args.Caption;
                        bRefresh = true;
                    }

                    //                     if (bRefresh)
                    //                     {
                    //                         Application.DoEvents();
                    //                     }
                });
            };
            App.Singleton<ProgressProxy>().OnSetValue += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    TaskbarManager.Instance.SetProgressValue(args.Value, 100, this.Handle);
                });
            };
            App.Singleton<ProgressProxy>().OnClear += (sender, args) =>
            {
                this.SafeInvoke(() =>
                {
                    TaskbarManager.Instance.SetProgressValue(0, 100, this.Handle);

                    tbInfo.Text = "就绪";
                    //tsProgressBar.Value = 0;
                    //statusStrip.Refresh();
                    //Application.DoEvents();
                });
            };
        }

        #region IWindow

        public string Title
        {
            get => this.Text;
            set => this.Text = value;
        }

        /// <summary>图标.</summary>
        /// <value>The icon.</value>
        public object IconImage
        {
            get => this.Icon;
            set => this.Icon = value as Icon;
        }

        #endregion

        #region IMainWindow


        /// <summary>Gets the active view window.</summary>
        /// <value>The active view window.</value>
        public object ActiveViewWindow => this;

        /// <summary>更新UI.</summary>
        public void RefreshUI()
        {
            this.Invalidate();
        }

        /// <summary>Occurs when [view window active].</summary>
        public event Action<IViewWindow> ViewWindowActive;

        #endregion

        #region IViewWindow
        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <value>The parent.</value>
        public object ParentWindow => this.Owner;

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public object View => osgEarthView;

        #endregion
    }
}
