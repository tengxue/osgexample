﻿// ***********************************************************************
// Assembly         : TxOsgViewer
// Author           : tianteng
// Created          : 07-01-2019
//
// Last Modified By : tianteng
// Last Modified On : 05-09-2020
// ***********************************************************************
// <copyright file="Program.cs" company="北京腾雪科技有限责任公司">
//     Copyright © 北京腾雪科技有限责任公司 2022
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Tx;
using Tx.OSG.UI;
using Tx.OSG;
using TxOsgViewer.Properties;
using MessageBoxButtons = System.Windows.Forms.MessageBoxButtons;
using Tx.UI;
using OsgCommon.Cmds;

namespace TxOsgViewer
{
    /// <summary>
    /// Class Program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //OsgDB.Registry.instance().ty
            var commandArgs = new CmdLineArgs(args);

            var osgViewForm = new SimpleOsgViewForm();
            osgViewForm.Icon = Resources.腾雪科技;
            osgViewForm.Text = "OSG查看器";
            InitMenu(osgViewForm.menuStrip, osgViewForm.App);

            ReadInFile(osgViewForm.ViewCtrl.OsgObj, commandArgs);

            Application.Run(osgViewForm);
        }

        static Program()
        {
            OsgConfiguration.Configure();
        }

        private static void InitMenu(MenuStrip menuStrip, AppService appService)
        {
            menuStrip.Items.AddFileMenu(appService);
            menuStrip.Items.AddViewMenu(appService);

            menuStrip.Items.Item<ToolStripMenuItem>("测试(&T)").DropDownItems
                .AddItem<ToolStripMenuItem>("创建立方体", appService, GeometryCmds.NewBox)
                .AddItem<ToolStripMenuItem>("创建面", appService, GeometryCmds.NewPolygon)
                .AddItem<ToolStripMenuItem>("拉伸矩形环", appService, GeometryCmds.NewRectAnnulus)
                .AddItem<ToolStripMenuItem, MatrixTransformTestCmd>("测试位置变换", appService)
                .AddItem<ToolStripMenuItem, LoadOsgFileCmd>("导入osg", appService);
        }

        private static void ReadInFile(OsgObj osgObj, CmdLineArgs commandArgs)
        {
            var osgFileName = commandArgs.Params.FirstOrDefault();
            if (File.Exists(osgFileName))
            {
                try
                {
                    var node = OsgDBEx.ReadNodeFile(osgFileName);
                    if (node.IsValid())
                    {
                        node.Name = Path.GetFileNameWithoutExtension(osgFileName);
                        osgObj.GroupFolder.UpdateChild("Models", new[] { node });
                        osgObj.Viewer.Show(node.Bound, ViewMode.ShowAll);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.GetAllExMessage(), "打开文件错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
